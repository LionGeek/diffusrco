<?php

define('PATH',getcwd());

require_once(PATH.'/includes/constants.php');
require_once(PATH.'/includes/helpers.php');
require_once(PATH.'/includes/Auth.php');
require_once(PATH.'/includes/fb/facebook.php');

$facebook = new Facebook(array(
  'appId'  => FACEBOOK_APP_ID,
  'secret' => FACEBOOK_SECRET
));

$auth = new Auth($facebook);
if(defined('DEBUG')){
	$auth->debug = true;
}
$check = $auth->check();
if($check['status'] != 0){
	// TODO: proper errors
	echo 'Facebook login failed. Please refresh the page and try again';
	die();
}

if(!$auth->apiUser && $auth->fbUser){
	$reg = $auth->register();
    if($reg['status'] != 0){
        // TODO: proper errors
        echo 'Registration error: '.$reg['error'];
        die();
    }
}

controller('index',array(
	'auth' => $auth,
	'facebook' => $facebook
));
