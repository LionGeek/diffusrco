<?php

if(!empty($_GET['action']) && $_GET['action']=='logout'){
	setcookie('fbs_'.$facebook->getAppId(), '', time()-100, '/', '');
	session_destroy();
	header("Location: index.php");
}

controller('header');
if($auth->fbUser && $auth->userActive){
	controller('main',array(
		'auth' => $auth
	));
}else if($auth->fbUser && !$auth->userActive){
	controller('landing-reserved');
}else if(!$auth->fbUser && !$auth->userActive){
	controller('landing',array(
		'fbLoginUrl' => $facebook->getLoginUrl(array('scope'=>'email'))
	));
}
controller('footer');
