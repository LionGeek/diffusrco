<?php

require_once(PATH.'/api/models/Groups.php');
$groups = new Groups();

$theGroups = $groups->getGroups();
$theUserGroups = $groups->getGroupsForUser($auth->apiUser->id);

view('main',array(
	'groups' => $theGroups['groups'],
	'groupsForUser' => $theUserGroups['groups'],
	'auth' => $auth
));
