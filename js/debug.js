function Debug() {
	this.allBroadcasts = false;
	this.debugElem = $('#debug');
	this.showAllBroadcasts = function(){
		this.allBroadcasts = true;
		refreshBroadcasts();
		this.debugElem.show().append('<div id="debug-showall">Showing all broadcasts</div>');
	};
}

var debug = new Debug();
