var latlng, mapOptions, map, g_position, oms;

$(function(){
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(success, error);
	} else {
		error("Geolocation on your device is disabled or not supported.");
	}
});

function error(msg) {
	var s = document.querySelector('#status');
	s.innerHTML = typeof msg == 'string' ? msg : "Geolocation on your device is disabled or not supported.";
	s.className = 'fail';
}

function centerMap(lat,lng){
	map.setCenter(new google.maps.LatLng(lat, lng), 5);
}

function requestAndUpdateBroadcastsListWithCurrentPosition(){
	var coords='coords/';
	if(debug.allBroadcasts){
		coords = '';
	}
	$.getJSON('api/broadcasts/' + coords + g_position.coords.latitude + "," + g_position.coords.longitude, function(data){
		updateBroadcastsListFromData($("#broadcasts-list-nearby"),data);
	});
}

function refreshBroadcasts(){
	requestAndUpdateBroadcastsListWithCurrentPosition();
}

function success(position) {
	g_position = position;
	var s = document.querySelector('#status');
	
	if (s.className == 'success') {
		return; // not sure why we're hitting this twice in FF, I think it's to do with a cached result coming back		 
	}
	
	s.innerHTML = "";
	s.className = 'success';
	
	var map_canvas = document.createElement('div');
	map_canvas.id = 'map_canvas';
	document.querySelector('article').appendChild(map_canvas);
	
	// Get the polar coordinates
	latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

	// Attribute the latitude and longitude to the "Adauga anunt"
	$("#sendBcLat").attr('value',position.coords.latitude);
	$("#sendBcLon").attr('value',position.coords.longitude);
	$("#sendBcSubmit").removeAttr('disabled');

	mapOptions = {
		zoom: 14,
		center: latlng,
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		zoomControl: true
	};

	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	oms = new OverlappingMarkerSpiderfier(map);

	oms.addListener('click', function(marker) {
		closeAllIbs();
		ibs[marker.markerN].open(map,marker);
	});
	oms.addListener('spiderfy', function(markers) {
		closeAllIbs();
	});
	google.maps.event.addListener(map, 'click', function(event) {
		closeAllIbs();
	});

	// var content =	'<div class="content clearfix">'+
	// 					'<img src="'+currUserPic+'" class="pull-left">'+
	// 					'<div class="pull-left" style="margin-left:10px;">Salut, '+currUserName+'!<br>Esti aici.</div>'+
	// 				'</div>';

	makeMarker(latlng, map, false, {}, '', null, 'img/current-location_15px.png');
	requestAndUpdateBroadcastsListWithCurrentPosition();
}
