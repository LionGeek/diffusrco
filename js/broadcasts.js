function updateBroadcastsListFromData(elem,data){
	elem.empty();
	removeAllMarkers();
	if(data.broadcasts.length > 0){
		for(i in data.broadcasts){
			var bc = data.broadcasts[i];
			var pos = new google.maps.LatLng(bc.center_latitude, bc.center_longitude);

			var bcClass = '';
			var groupText = '';
			var groupTextWithLabel = '';

			if(bc.group_name != null && bc.group_name.length > 0){
				groupText = bc.group_name;
				groupTextWithLabel = 'in <span class="label label-info">'+bc.group_name+'</span>';
				bcClass += 'group'+bc.group_id+' ';
			}

			if(bc.category_name != null && bc.category_name.length > 0){
				bcClass += 'category'+bc.category_id+' ';
			}
			var excerpt = bc.description;
			var limit = 75;
			if(excerpt.length>limit){
				excerpt = excerpt.substr(0,(parseInt(limit)-3))+'...';
			}

			var lat1 = bc.center_latitude;
			var lon1 = bc.center_longitude;
			var lat2 = g_position.coords.latitude;
			var lon2 = g_position.coords.longitude;

			var d = bc.distance;
			var um = '';
			if(d < 1.0){
				um = 'm';
				d *= 1000;
				d = Math.round(d);
			}else{
				um = 'km';
				d = d.toFixed(1);
			}

			var creation_date = Date.parse(bc.creation_date);
			var creation_date_js = new Date(creation_date.toString());
			var creation_date_iso = creation_date.toISOString();
			var creation_date_human = creation_date.toString('d-MMM-yyyy HH:mm');
			var creation_date_timeago = $.timeago(creation_date_iso);

			var expiration_date = Date.parse(bc.expiration_date);
			var expiration_date_js = new Date(expiration_date.toString());
			var expiration_date_iso = expiration_date.toISOString();
			var expiration_date_human = expiration_date.toString('d-MMM-yyyy HH:mm');
			var expiration_date_timeago = $.timeago(expiration_date_iso);

			var timeToEnd = Math.abs(expiration_date_js - new Date());
			var timeFromStart = Math.abs(new Date() - creation_date_js);

			var hoursToEnd = Math.round(timeToEnd/1000/60/60);
			var hoursFromStart = Math.round(timeFromStart/1000/60/60);

			var markerContent =	'<div class="broadcast">'+
									'<div class="content">'+
										'<div class="top">'+
											'<h3>'+bc.user_name+'</h3>'+
											'<div class="subtitle">'+
												'<abbr class="timeago" title="'+creation_date_iso+'">acum '+hoursFromStart+'h</abbr> cu '+
												'<abbr class="timeago" title="'+expiration_date_iso+'">'+hoursToEnd+'h ramase</abbr> '+
												groupTextWithLabel+
											'</div>'+
										'</div>'+
										'<div class="description">'+
											'<p>'+bc.description+'</p>'+
										'</div>'+
									'</div>'+
									'<div class="details clearfix">'+
										'<div class="distance pull-left">'+ d + um +'</div>'+
										'<div class="contact pull-right"><button class="btn btn-primary btn-small">Contact</button></div>'+
									'</div>'+
								'</div>';

			mId = makeMarker(pos, map, true, {}, markerContent);

			elem.append(''+
				'<div class="broadcast '+bcClass+'" data-id="'+mId+'">'+
					'<a href="javascript:;">'+
						'<div>'+
							'<div class="content">'+
								'<span class="username">'+bc.user_name+'</span> '+excerpt+
							'</div>'+
							'<div class="details clearfix">'+
								'<div class="distance pull-left">'+ d + um +'</div>'+
								'<div class="group pull-right">'+groupTextWithLabel+'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'<hr>'+
				'</div>'+
			'');
		}
		$('.broadcasts-list > .broadcast > a').click(function(){
			var id = $(this).parent().attr('data-id');
			if(id in markers){
				var marker = markers[id];
				centerMap(marker.position.jb, marker.position.kb);
			}
			if(id in ibs){
				var ib = ibs[id];
				closeAllIbs();
				ib.open(map, marker);
			}
		});
	}else{
		elem.append('Scuze, nu exista anunturi langa tine. Fi primul care sa faca un anunt!');
	}
}

function requestAndUpdateBroadcastsListWithGroup(group){
	$.getJSON('api/broadcasts/group/' + g_position.coords.latitude + ',' + g_position.coords.longitude + ',' + group, function(data){
		updateBroadcastsListFromData($("#broadcasts-list-groups"),data);
	});
}
