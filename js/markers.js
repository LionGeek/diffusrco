var ibWidth = 350;
var ibOptionsDefault = {
	alignBottom: true,
	disableAutoPan: false,
	maxWidth: 0,
	zIndex: null,
	pixelOffset: new google.maps.Size(-1 * (ibWidth/2), -60),
	closeBoxMargin: "10px",
	closeBoxURL: "img/transparent.gif",
	infoBoxClearance: new google.maps.Size(1, 1),
	pane: "floatPane",
	enableEventPropagation: false,
	boxStyle:{ 
		width: ibWidth+'px'
	}
}

var ibs = [];
var markers = [];
var markerN = 0;

function closeAllIbs(){
	for(i in ibs){
		ibs[i].close();
	}
}

function removeAllMarkers(){
	for(i in markers){
		if(i==0) continue;
		markers[i].setMap(null);
	}
}

function makeMarker(pos, map, ib, ibOptions, divContent, divClass, icon){
	if(divClass == undefined || divClass == null){
		divClass = '';
	}else{
		divClass = ' ' + divClass;
	}

	var marker = new google.maps.Marker({
			position: pos, 
			map: map
	});

	if(icon != undefined){
		marker.setIcon(icon);
	}

	markers[markerN] = marker;

	if(ib){
		for(prop in ibOptionsDefault) { 
			if(prop in ibOptions) { continue; }
			ibOptions[prop] = ibOptionsDefault[prop];
		}

		var ibDiv = document.createElement('div');
		ibDiv.innerHTML = divContent;
		ibDiv.className = 'infobox-content' + divClass;

		ibOptions.content = ibDiv;

		marker.markerN = markerN;
		ibs[markerN] = new InfoBox(ibOptions);

		oms.addMarker(marker);
	}

	markerN++;

	return markerN-1;

	// google.maps.event.addListener(marker, 'click', function(){
	// 	for(i in ibs){
	// 		ibs[i].close();
	// 	}
	// 	ibs[this.ibN].open(map,this);
	// });
}
