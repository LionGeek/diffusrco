$(function(){
	$('#sendBcGroup').select2();
	$("#sendBcSubmit").click(function(){
		data = {};
		data.center_latitude = $('#sendBcLat').val();
		data.center_longitude = $('#sendBcLon').val();
		data.radius = $('#sendBcRadius').val();
		data.expiration_date = $('#sendBcUntil').val();
		data.description = $('#sendBcDescription').val();
		data.group = $('#sendBcGroup :selected').val().substr(5);
		$.post('ajax/send_broadcast.php', data,
			function(data){
				if(data.status != 0){
					$('#sendBcError').html(data.error).show();
				}else{
					$('#adaugaAnuntModal').modal('hide');
					$('#sendBcDescription').val('');
					refreshBroadcasts();
				}
			}, 'json'
		);
	});

	$('#groups-search').select2();
	$('#groups-search').change(function(){
		var val = $(this).select2('val');
		requestAndUpdateBroadcastsListWithGroup(val.substr(5));
	});
	
	$('#sidebar-nearby-section-trigger').click(function(){
		$('.sidebar-section').hide();
		$('#sidebar-nearby-section').show();
		$('.sidebar-section-trigger').parent().removeClass('current');
		$(this).parent().addClass('current');
	});
	$('#sidebar-groups-section-trigger').click(function(){
		$('.sidebar-section').hide();
		$('#sidebar-groups-section').show();
		$('.sidebar-section-trigger').parent().removeClass('current');
		$(this).parent().addClass('current');
	});
	$('#sidebar-search-section-trigger').click(function(){
		$('.sidebar-section').hide();
		$('#sidebar-search-section').show();
		$('.sidebar-section-trigger').parent().removeClass('current');
		$(this).parent().addClass('current');
	});

	$('.nav-dropdown-container .nav-dropdown-trigger').click(function(){
		$(this).parent().find('.nav-dropdown').toggle();
	});
	$('.nav-dropdown li a').click(function(){
		$(this).parent().parent().hide();
	});
});
