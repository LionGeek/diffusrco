<?php

require_once(PATH.'/includes/helpers.php');
require_once(PATH.'/api/models/Groups.php');
require_once(PATH.'/api/models/Users.php');
require_once(PATH.'/api/models/Vips.php');

class Auth {
	public $debug;
	public $facebook;
	public $groups;
	public $users;
	public $vips;

	public $userActive;
	public $apiUserId;
	public $fbUser;
	public $fbUserProfile;
	public $apiUser;
	public $apiVip;
	public function __construct($facebook){
		$this->facebook = $facebook;
		$this->groups = new Groups();
		$this->users = new Users();
		$this->vips = new Vips();
		$this->userActive = 0;
		$this->apiUserId = 0;
		$this->fbUser = false;
		$this->fbUserProfile = false;
		$this->apiUser = false;
		$this->apiVip = false;
		$this->debug = false;
	}
	public function check() {
		$ret = array('status'=>0);
		if($this->debug){
			$this->fbUser = 'au.razvan';
			$this->userActive = 1;
			$this->fbUserProfile = array();
			$this->fbUserProfile['name'] = 'Razvan Aurariu';
			$this->apiUser = new stdClass;
			$this->apiUser->id = 118;
			return $ret;
		}

		$this->fbUser = $this->facebook->getUser();
		if($this->fbUser){
			try {
				$apiUserResp = $this->users->getFbUser($this->fbUser);
				$this->apiUser = $apiUserResp['user'];
				$this->fbUserProfile = $this->facebook->api('/me');
				if(empty($this->fbUserProfile['email'])){
					$ret['status'] = 1;
					$ret['error'] = 'Could not get Facebook info.';
					return $ret;
				}

				if($this->apiUser){
					$this->userActive = (int)$this->apiUser->active;
				}else{
					$apiVipResp = $this->vips->getVipByEmail($this->fbUserProfile['email']);
					$this->apiVip = $apiVipResp['vip'];
				}
			}catch (FacebookApiException $e){
				$this->fbUser = false;
				$ret['status'] = 2;
				$ret['error'] = 'Facebook error: '.$e;
				return $ret;
			}
		}

		return $ret;
	}
	public function register() {
		$ret = array('status'=>0);
		if(empty($this->fbUserProfile['email'])){
			$ret['status'] = 1;
			$ret['error'] = 'Could not get Facebook info.';
			return $ret;
		}

		if($this->apiVip){
			$active = true;
		}else{
			$active = false;
		}

		$addUser = $this->users->addUser(array(
			'email'=>$this->fbUserProfile['email'],
			'name'=>$this->fbUserProfile['name'],
			'fb_id'=>$this->fbUserProfile['id'],
			'active'=>$active
		));

		if($addUser['status'] != 0){
			$ret['status'] = $addUser['status'];
			$ret['error'] = $addUser['error'];
			return $ret;
		}
		
		$this->userActive = 1;
		return $ret;
	}
}
