<?php

function do_post_request($url, $data, $optional_headers = null) {
	$params = array('http' => array(
		'method' => 'POST',
		'content' => $data
	));
	if ($optional_headers !== null) {
		$params['http']['header'] = $optional_headers;
	}
	$ctx = stream_context_create($params);
	$fp = @fopen($url, 'rb', false, $ctx);
	$response = @stream_get_contents($fp);
	return $response;
}

function view($__view_name, $__vars = array()) {
	extract($__vars);
	require(PATH.'/views/'.$__view_name.'.php');
}

function controller($__controller_name, $__vars = array()) {
	extract($__vars);
	require(PATH.'/controllers/'.$__controller_name.'.php');
}

function postEmpty($var){
	return empty($_POST[$var]);
}
