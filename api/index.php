<?php
ini_set("display_errors", 1);
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';
require_once 'coords.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */

$appConfig = array();

$app = new \Slim\Slim(array(
	'config' => $appConfig
));


/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, and `Slim::delete`
 * is an anonymous function.
 */

require_once('models/Users.php');
require_once('models/Vips.php');
require_once('models/Broadcasts.php');
require_once('models/Groups.php');
require_once('models/Groups_Users.php');

// Users
$users = new Users();
$app->get('/users', 'getUsers');
$app->get('/users/:id', 'getUser');
$app->get('/users/fb/:id', 'getFbUser');
$app->post('/users', 'addUser');
function getUsers(){ global $users; echo json_encode($users->getUsers()); }
function getUser($id){ global $users; echo json_encode($users->getUser($id)); }
function getFbUser($id){ global $users; echo json_encode($users->getFbUser($id)); }
function addUser(){ global $users; echo json_encode($users->addUser()); }

// VIPs
$vips = new Vips();
$app->get('/vips', 'getVips');
$app->get('/vips/:id', 'getVip');
$app->get('/vips/email/:email', 'getVipByEmail');
$app->post('/vips', 'addVip');
function getVips(){ global $vips; echo json_encode($vips->getVips()); }
function getVip($id){ global $vips; echo json_encode($vips->getVip($id)); }
function getVipByEmail($email){ global $vips; echo json_encode($vips->getVipByEmail($email)); }
function addVip(){ global $vips; echo json_encode($vips->addVip()); }

// Broadcasts
$broadcasts = new Broadcasts();
$app->get('/broadcasts/:lat,:lng', 'getBroadcasts');
$app->get('/broadcasts/:id', 'getBroadcast');
$app->get('/broadcasts/coords/:lat,:lng', 'getBroadcastsByCoords');
$app->get('/broadcasts/group/:lat,:lng,:group', 'getBroadcastsByGroup');
$app->post('/broadcasts', 'addBroadcast');
function getBroadcasts($lat,$lng){ global $broadcasts; echo json_encode($broadcasts->getBroadcasts($lat,$lng)); }
function getBroadcast($id){ global $broadcasts; echo json_encode($broadcasts->getBroadcast($id)); }
function getBroadcastsByCoords($lat,$lng){ global $broadcasts; echo json_encode($broadcasts->getBroadcastsByCoords($lat,$lng)); }
function getBroadcastsByGroup($lat,$lng,$group){ global $broadcasts; echo json_encode($broadcasts->getBroadcastsByGroup($lat,$lng,$group)); }
function addBroadcast(){ global $broadcasts; echo json_encode($broadcasts->addBroadcast()); }

// Groups
$groups = new Groups();
$app->get('/groups', 'getGroups');
$app->get('/groups/:id', 'getGroup');
$app->post('/groups', 'addGroup');
function getGroups(){ global $groups; echo json_encode($groups->getGroups()); }
function getGroup($id){ global $groups; echo json_encode($groups->getGroup($id)); }
function addGroup(){ global $groups; echo json_encode($groups->addGroup()); }

// Groups_Users
$groups_users = new Groups_Users();
$app->get('/groups_users/:user,:group', 'userInGroup');
$app->post('/groups_users', 'addUserToGroup');
$app->delete('/group_users/:user,:group', 'removeUserFromGroup');
function userInGroup($user,$group){ global $groups_users; echo json_encode($groups_users->userInGroup($user,$group)); }
function addUserToGroup(){ global $groups_users; echo json_encode($groups_users->addUserToGroup()); }
function removeUserFromGroup($user,$group){ global $groups_users; echo json_encode($groups_users->removeUserFromGroup($user,$group)); }

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
