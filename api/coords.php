<?php
function getBoundingCoords( $centerLat, $centerLong, $distance ) {
	$top = maxLatLongOnBearing($centerLat, $centerLong, 45, $distance);
	$right = maxLatLongOnBearing($centerLat, $centerLong, 135, $distance);
	$bottom = maxLatLongOnBearing($centerLat, $centerLong, 225, $distance);
	$left = maxLatLongOnBearing($centerLat, $centerLong, 315, $distance);

	$retArray = array( $top['maxLat'], $top['maxLon'], $bottom['maxLat'], $bottom['maxLon'] );

	return $retArray;	
}

function maxLatLongOnBearing( $centerLat, $centerLong, $bearing, $distance) {
	$lonRads = deg2rad($centerLong);
	$latRads = deg2rad($centerLat);
	$bearingRads = deg2rad($bearing);
	$maxLatRads = asin(sin($latRads) * cos($distance / 6371) + cos($latRads) * sin($distance/6371)*cos($bearingRads));
	$maxLonRads = $lonRads + atan2((sin($bearingRads) * sin($distance/6371) * cos($latRads)), (cos($distance/6371) - sin($latRads) * sin($maxLatRads)));

	$maxLat = rad2deg($maxLatRads);
	$maxLon = rad2deg($maxLonRads);

	$retArray = array('maxLat' => $maxLat,
			  'maxLon' => $maxLon );

	return $retArray;
}

function distanceBetween($lat1, $lon1, $lat2, $lon2) {
	$R = 6371;
	$dLat = deg2rad($lat2-$lat1);
	$dLon = deg2rad($lon2-$lon1); 
	$a =sin($dLat/2) * sin($dLat/2) +
		cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * 
		sin($dLon/2) * sin($dLon/2); 
	$c = 2 * atan2(sqrt($a), sqrt(1-$a)); 
	$d = $R * $c;
	return $d;
}

function cmpBroadcastsByDistance($b1,$b2){
	if($b1->distance == $b2->distance){
		return 0;
	}
	return ($b1->distance > $b2->distance) ? 1 : -1;
}
