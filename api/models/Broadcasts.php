<?php

if(!defined('PATH')){
    define('PATH',realpath(getcwd().'/../'));
}

require_once(PATH.'/api/models/DbConnection.php');
require_once(PATH.'/api/coords.php');

class Broadcasts {
	public $db;
	public function __construct(){
		$this->db = DbConnection::get();
	}
	function getBroadcasts($lat = 'none', $lng = 'none') {
		$sql = "SELECT broadcasts.id, user_id, description, group_id, broadcasts.creation_date, expiration_date,
		center_latitude, center_longitude, radius, ur_latitude, ur_longitude dl_latitude, dl_longitude, 
		users.name AS user_name, users.fb_id AS user_fb_id, users.email AS user_email, groups.name AS group_name
		FROM broadcasts
		JOIN users
		ON broadcasts.user_id = users.id
		LEFT JOIN groups
		ON broadcasts.group_id = groups.id";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->query($sql);
			$broadcasts = $stmt->fetchAll(PDO::FETCH_OBJ);
			$ret['broadcasts'] = $broadcasts;
			if($lat != 'none' && $lng != 'none'){
				foreach($ret['broadcasts'] as &$broadcast){
					$broadcast->distance = distanceBetween($lat,$lng,$broadcast->center_latitude,$broadcast->center_longitude);
				}
				usort($ret['broadcasts'],"cmpBroadcastsByDistance");
			}
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	function getBroadcast($id) {
		$sql = "SELECT * FROM broadcasts WHERE id = :id";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("id", $id);
			$stmt->execute();
			$ret['broadcast'] = $stmt->fetchObject();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	function getBroadcastsByCoords($lat,$lng) {
		$ret = array('status'=>0);
		$sql = "SELECT broadcasts.id, user_id, description, group_id, broadcasts.creation_date, expiration_date,
		center_latitude, center_longitude, radius, ur_latitude, ur_longitude dl_latitude, dl_longitude, 
		users.name AS user_name, users.fb_id AS user_fb_id, users.email AS user_email, groups.name AS group_name
		FROM broadcasts
		JOIN users
		ON broadcasts.user_id = users.id
		LEFT JOIN groups
		ON broadcasts.group_id = groups.id
		WHERE ur_latitude > :latitude and dl_latitude < :latitude and ur_longitude > :longitude and dl_longitude < :longitude";
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("latitude", $lat);
			$stmt->bindParam("longitude", $lng);
			$stmt->execute();
			$broadcasts = $stmt->fetchAll(PDO::FETCH_OBJ);
			$ret['broadcasts'] = $broadcasts;
			foreach($ret['broadcasts'] as &$broadcast){
				$broadcast->distance = distanceBetween($lat,$lng,$broadcast->center_latitude,$broadcast->center_longitude);
			}
			usort($ret['broadcasts'],"cmpBroadcastsByDistance");
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	function getBroadcastsByGroup($lat,$lng,$group) {
		$ret = array('status'=>0);
		$sql = "SELECT broadcasts.id, user_id, description, group_id, broadcasts.creation_date, expiration_date,
		center_latitude, center_longitude, radius, ur_latitude, ur_longitude dl_latitude, dl_longitude, 
		users.name AS user_name, users.fb_id AS user_fb_id, users.email AS user_email, groups.name AS group_name
		FROM broadcasts
		JOIN users
		ON broadcasts.user_id = users.id
		LEFT JOIN groups
		ON broadcasts.group_id = groups.id
		WHERE (ur_latitude > :latitude and dl_latitude < :latitude and ur_longitude > :longitude and dl_longitude < :longitude) AND broadcasts.group_id = :group";
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("latitude", $lat);
			$stmt->bindParam("longitude", $lng);
			$stmt->bindParam("group", $group);
			$stmt->execute();
			$broadcasts = $stmt->fetchAll(PDO::FETCH_OBJ);
			$ret['broadcasts'] = $broadcasts;
			foreach($ret['broadcasts'] as &$broadcast){
				$broadcast->distance = distanceBetween($lat,$lng,$broadcast->center_latitude,$broadcast->center_longitude);
			}
			usort($ret['broadcasts'],"cmpBroadcastsByDistance");
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	function addBroadcast($params = array()) {
        if(empty($params)){
            $request = \Slim\Slim::getInstance()->request();
            $broadcast = json_decode($request->getBody());
        }else{
			// convert array to object (because it's accessed like an object a few lines down)
			// OPTIMIZATION: convert the array by actually iterating through it (less clever)
			$broadcast = json_decode(json_encode($params, FALSE));
		}
        $sql = "INSERT INTO broadcasts (user_id, description, group_id, creation_date, expiration_date, center_latitude, center_longitude, radius, ur_latitude, ur_longitude, dl_latitude, dl_longitude) VALUES  (:user_id, :description, :group_id, :creation_date, :expiration_date, :center_latitude, :center_longitude, :radius, :ur_latitude, :ur_longitude, :dl_latitude, :dl_longitude)";
		list($ur_lat, $ur_long, $dl_lat, $dl_long) = getBoundingCoords($broadcast->center_latitude, $broadcast->center_longitude, $broadcast->radius);
		$ret = array('status'=>0);
        if(empty($broadcast->group) || $broadcast->group==0){
            $broadcast->group = null;
        }
		try {
			$creation_date = date("Y-m-d H:i:s", $broadcast->creation_date);
			$expiration_date = date("Y-m-d H:i:s", $broadcast->expiration_date);
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("user_id", $broadcast->user_id);
			$stmt->bindParam("description", $broadcast->description);
            $stmt->bindParam("group_id", $broadcast->group);
            $stmt->bindParam("creation_date", $creation_date);
            $stmt->bindParam("expiration_date", $expiration_date);
			$stmt->bindParam("center_latitude", $broadcast->center_latitude);
			$stmt->bindParam("center_longitude", $broadcast->center_longitude);
			$stmt->bindParam("radius", $broadcast->radius);
			$stmt->bindParam("ur_latitude", $ur_lat);
			$stmt->bindParam("ur_longitude", $ur_long);
			$stmt->bindParam("dl_latitude", $dl_lat);
			$stmt->bindParam("dl_longitude", $dl_long);
			$stmt->execute();
			$ret['id'] = $this->db->lastInsertId();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
}
