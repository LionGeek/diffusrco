<?php

require_once('DbConnection.php');

class Groups {
	public $db;
	public function __construct(){
		$this->db = DbConnection::get();
	}
	public function getGroups() {
		$sql = "SELECT * FROM groups";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->query($sql);
			$groups = $stmt->fetchAll(PDO::FETCH_OBJ);
			$ret['groups'] = $groups;
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	public function getGroupsForUser($user) {
		$sql = "SELECT * FROM groups
		JOIN groups_users
		ON groups_users.group = groups.id
		WHERE groups_users.user = :user";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("user", $user);
			$stmt->execute();
			$groups = $stmt->fetchAll(PDO::FETCH_OBJ);
			$ret['groups'] = $groups;
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	public function getGroup($id) {
		$sql = "SELECT * FROM groups where id = :id";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("id", $id);
			$stmt->execute();
			$ret['group'] = $stmt->fetchObject();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}

	public function addGroup() {
		$request = \Slim\Slim::getInstance()->request();
		$group = json_decode($request->getBody());
		$sql = "INSERT INTO groups (name) VALUES (:name)";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("name", $group->name);
			$stmt->execute();
			$ret['id'] = $this->db->lastInsertId();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
}
