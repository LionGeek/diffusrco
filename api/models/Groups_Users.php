<?php

require_once('DbConnection.php');

class Groups_Users {
	public $db;
	public function __construct(){
		$this->db = DbConnection::get();
	}

	public function userInGroup($user,$group) {
		$sql = "SELECT * FROM groups_users WHERE `user` = :user AND `group` = :group";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("user", $user);
			$stmt->bindParam("group", $group);
			$stmt->execute();
			if($stmt->rowCount() > 0){
				$ret['userInGroup']=1;
			}else{
				$ret['userInGroup']=0;
			}
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}

	public function addUserToGroup() {
		$request = \Slim\Slim::getInstance()->request();
		$resp = json_decode($request->getBody());
		$sql = "INSERT IGNORE INTO groups_users (`user`,`group`) VALUES (:user,:group)";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("user", $resp->user);
			$stmt->bindParam("group", $resp->group);
			$stmt->execute();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}

	public function removeUserFromGroup($user,$group) {
		$sql = "DELETE FROM groups_users WHERE `user` = :user AND `group` = :group";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("user", $user);
			$stmt->bindParam("group", $group);
			$stmt->execute();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
}
