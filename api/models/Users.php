<?php

require_once('DbConnection.php');

class Users {
	public $db;
	public function __construct(){
		$this->db = DbConnection::get();
	}
	public function getUsers() {
		$sql = "SELECT * FROM users";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->query($sql);
			$users = $stmt->fetchAll(PDO::FETCH_OBJ);
			$ret['users'] = $users;
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	public function getUser($id) {
		$sql = "SELECT * FROM users where id = :id";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("id", $id);
			$stmt->execute();
			$ret['user'] = $stmt->fetchObject();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	public function getFbUser($id) {
		$sql = "SELECT * FROM users where fb_id = :id";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("id", $id);
			$stmt->execute();
			$ret['user'] = $stmt->fetchObject();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	public function addUser($params = array()) {
		if(empty($params)){
			$request = \Slim\Slim::getInstance()->request();
			$user = json_decode($request->getBody());
		}else{
			// convert array to object (because it's accessed like an object a few lines down)
			// OPTIMIZATION: convert the array by actually iterating through it (less clever)
			$user = json_decode(json_encode($params, FALSE));
		}
		$sql = "INSERT INTO users (email, name, fb_id, active, creation_date) VALUES (:email, :name, :fb_id, :active, :creation_date)";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("email", $user->email);
			$stmt->bindParam("name", $user->name);
			$stmt->bindParam("fb_id", $user->fb_id);
			$stmt->bindParam("active", $user->active);
			$stmt->bindParam("creation_date", time());
			$stmt->execute();
			$ret['id'] = $this->db->lastInsertId();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
}
