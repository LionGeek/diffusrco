<?php

require_once('DbConnection.php');

class Vips {
	public $db;
	public function __construct(){
		$this->db = DbConnection::get();
	}
	public function getVips() {
		$sql = "SELECT * FROM vips";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->query($sql);
			$vips = $stmt->fetchAll(PDO::FETCH_OBJ);
			$ret['vips'] = $vips;
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
	public function getVip($id) {
		$sql = "SELECT * FROM vips where id = :id";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("id", $id);
			$stmt->execute();
			$ret['vip'] = $stmt->fetchObject();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}

	public function getVipByEmail($email) {
		$sql = "SELECT * FROM vips where email = :email";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("email", $email);
			$stmt->execute();
			$ret['vip'] = $stmt->fetchObject();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}

	public function addVip() {
		$request = \Slim\Slim::getInstance()->request();
		$vip = json_decode($request->getBody());
		$sql = "INSERT INTO vips (email) VALUES (:email)";
		$ret = array('status'=>0);
		try {
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam("email", $vip->email);
			$stmt->execute();
			$ret['id'] = $this->db->lastInsertId();
			return $ret;
		} catch(PDOException $e) {
			$ret['status'] = 1;
			$ret['error'] = $e->getMessage();
			return $ret;
		}
	}
}
