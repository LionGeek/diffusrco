<div id="page-main">
	<div id="navbar">
		<div class="inner">
			<div class="nav-left pull-left">
				<a class="logo" href="#"><img src="img/logo_125px.png"></a>
			</div>
			<div class="nav-right pull-right clearfix">
				<div class="nav-right-add pull-left">
					<button class="btn btn-primary adauga-anunt-btn" href="#adaugaAnuntModal" role="button" data-toggle="modal">Adauga un anunt</button>
				</div>
				<div class="nav-right-settings nav-dropdown-container pull-left">
					<a href="#" class="nav-dropdown-trigger"><img src="img/settings_16px.png"></a>
					<ul class="nav-dropdown">
						<li><a href="#profilModal" data-toggle="modal">Profil</a></li>
						<li><a href="index.php?action=logout">Logout</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="map">
		<div class="inner">
			<article>
				<span id="status"></span>
			</article>
		</div>
	</div>
	<div id="sidebar">
		<div id="sidebar-spacer"></div>
		<div id="sidebar-nav">
			<div class="inner clearfix">
				<div class="section-button current">
					<a href="javascript:;" class="sidebar-section-trigger" id="sidebar-nearby-section-trigger">
						In Apropiere
					</a>
				</div>
				<div class="section-button">
					<a href="javascript:;" class="sidebar-section-trigger" id="sidebar-groups-section-trigger">
						Grupuri
					</a>
				</div>
				<div class="section-button">
					<a href="javascript:;" class="sidebar-section-trigger" id="sidebar-search-section-trigger">
						Cautare
					</a>
				</div>
			</div>
		</div>
		<div id="sidebar-content">
			<div id="sidebar-nearby-section" class="sidebar-section">
				<div class="inner">
					<div id="broadcasts-list-nearby" class="broadcasts-list">
					</div>
				</div>
			</div>
			<div id="sidebar-groups-section" class="sidebar-section hide">
				<div class="inner">
					<select name="groups-search" id="groups-search" placeholder="Alege un grup" style="width:100%;">
						<option value=""></option>
						<?php
						foreach($groups as $group){
							echo '<option value="group'.$group->id.'">'.$group->name.'</option>';
						}
						?>
					</select>
					<div id="broadcasts-list-groups" class="broadcasts-list">
					</div>
				</div>
			</div>
			<div id="sidebar-search-section" class="sidebar-section hide">
				<div class="inner">
					<select name="search-search" id="search-search" placeholder="Cauta un anunt dupa hashtag" style="width:100%;">
						<option value=""></option>
						<?php
						foreach($groups as $group){
							echo '<option value="group'.$group->id.'">'.$group->name.'</option>';
						}
						?>
					</select>
					<div id="broadcasts-list-search" class="broadcasts-list">
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- modals -->
	<div id="adaugaAnuntModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="adaugaAnuntModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="adaugaAnuntModalLabel">Adauga un anunt</h3>
		</div>
		<div class="modal-body">
			<form id="broadcastForm" method="post" action="includes/send_broadcast.php">
				<div class="alert alert-error hide" id="sendBcError"></div>
				<textarea id="sendBcDescription" rows="4" placeholder="Ce vrei sa transmiti?" maxlength="140" name="description"></textarea>
				<select name="group" id="sendBcGroup" style="width:100%;">
					<option value="0">Public</option>
					<?php
					foreach($groupsForUser as $group){
						echo '<option value="group'.$group->id.'">'.$group->name.'</option>';
					}
					?>
				</select>
				<div class="info">Anuntul tau va aparea pe o raza de <strong>500m</strong> pentru <strong>24 ore</strong>.</div>
				<input type="hidden" id="sendBcLat" name="center_latitude">
				<input type="hidden" id="sendBcLon" name="center_longitude">
				<input type="hidden" id="sendBcRadius" name="radius" value="0.5">
				<input type="hidden" id="sendBcUntil" name="expiration_date" value="<?php echo time() + 60*60*24; ?>">
			</form>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Anuleaza</button>
			<button class="btn btn-primary" id="sendBcSubmit" disabled="yep">Trimite anuntul</button>
		</div>
	</div>
	<div id="profilModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="profilModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="profilModalLabel">Profil</h3>
		</div>
		<div class="modal-body">
			Esti sexy.
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Okay</button>
		</div>
	</div>
	<div id="messageModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="messageModalLabel">Trimite un mesaj</h3>
		</div>
		<div class="modal-body">
			<p>Destinatar: <strong>Andrei Balan</strong></p>
			<textarea style="width:97%;" rows="3">Salut! Am vazut anuntul tau in legatura cu taxiul. Eu sunt in fata la Vintage Pub, si am un tricou rosu. Te astept!</textarea>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Anuleaza</button>
			<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Trimite</button>
		</div>
	</div>
	<!-- /modals -->

	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/oms.min.js"></script>
	<script type="text/javascript" src="js/date.js"></script>
	<script type="text/javascript" src="js/jquery.timeago.js"></script>
	<script type="text/javascript" src="js/select2.js"></script>
	<script type="text/javascript" src="js/helpers.js"></script>
	<script type="text/javascript" src="js/debug.js"></script>
	<script type="text/javascript" src="js/infobox.js"></script>
	<script type="text/javascript" src="js/triggers.js"></script>
	<script type="text/javascript" src="js/markers.js"></script>
	<script type="text/javascript" src="js/broadcasts.js"></script> 
	<script type="text/javascript" src="js/geo.js"></script> 
</div>
