<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>diffusr</title>
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="apple-touch-icon-precomposed" href="apple-touch-icon.png">
		<link rel="stylesheet" href="stylesheets/bootstrap.min.css">
		<link rel="stylesheet" href="stylesheets/bootstrap-responsive.min.css">
		<link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,900,800'>
		<link rel="stylesheet" href="stylesheets/select2.css">
		<link rel="stylesheet" href="stylesheets/style.css">
		<meta name="viewport" content="width=620">
	</head>
    <body>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
		<script>
		var currUserName = '<?php echo $currUserName; ?>';
		var currUserPic = '<?php echo $currUserPic; ?>';
		</script>
		<div id="debug"></div>

