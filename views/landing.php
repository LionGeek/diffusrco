<div id="page-landing">
	<img src="img/logo_350px.png" class="logo">

	<hr>

	<div class="heading clearfix">
		<div class="top clearfix">
			<div class="inner">
				<div class="word helping" >Helping&nbsp;</div>
				<div class="word students switcher">
					<div id="who">students</div>
				</div>
			</div>
		</div>
		<div class="bottom clearfix">
			<div class="inner">
				<div class="word socialise">socialise&nbsp;</div>
				<div class="word locally">locally.</div>
			</div>
		</div>
	</div>

	<a href="<?php echo $fbLoginUrl; ?>" class="login">
		<img src="img/connect_with_fb.png">
	</a>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
	<script>
	var who = ['students','companies','friends'];
	var curr = 0;
	function updateWho(){
		$("#who").fadeTo(600,0.01,function(){
			if(curr >= ((who.length)-1)){
				curr = 0;
			}else{
				curr += 1;
			}
			$(this).html(who[curr]);
			$(this).fadeTo(600,1);
		});
	}
	var i = setInterval(updateWho,2000);
	</script>
</div>
