<?php

define('PATH',realpath(getcwd().'/../'));

require_once(PATH.'/includes/constants.php');
require_once(PATH.'/includes/helpers.php');
require_once(PATH.'/includes/Auth.php');
require_once(PATH.'/includes/fb/facebook.php');
require_once(PATH.'/api/models/Broadcasts.php');

$ret = array();
$ret['status'] = 0;

function error($status, $error){
	global $ret;
	$ret['status'] = $status;
	$ret['error'] = $error;
	echo json_encode($ret);
	die();
}

$broadcasts = new Broadcasts();

$facebook = new Facebook(array(
  'appId'  => FACEBOOK_APP_ID,
  'secret' => FACEBOOK_SECRET
));

$auth = new Auth($facebook);
if(defined('DEBUG')){
	$auth->debug = true;
}
$auth->check();

if(!$auth->userActive || !$auth->apiUser){
	error(403,'Va rugam sa va autentificati.');
}

if(postEmpty('description')){
	error(1,'Va rugam sa completati descrierea.');
}
if(	postEmpty('center_latitude') || postEmpty('center_longitude') ||
	postEmpty('radius') || postEmpty('expiration_date')
){
	error(1,'Ne pare rau, dar nu v-am putut transmite datele. Va rugam sa <a href="contact">contactati un administrator</a>.');
}

// override fake options with defaults
$_POST['radius'] = 0.5;
$_POST['creation_date'] = time();
$_POST['expiration_date'] = (int)(time() + (60*60*24));

$_POST['user_id'] = $auth->apiUser->id;

$addBc = $broadcasts->addBroadcast($_POST);
if($addBc['status'] != 0){
	error($addBc['status'], $addBc['error']);
}

echo json_encode($ret);
die();
