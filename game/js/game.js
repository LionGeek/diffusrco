// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.id = "GameCanvas";
//canvas.style.display="none";
canvas.width = 512;
canvas.height = 480;
timeLeft = 45;
start = true;
started = false;
document.body.appendChild(canvas);

// Background image
var bgReady = false;
var bgImage = new Image();
bgImage.onload = function() { bgReady = true; };
bgImage.src = "images/image_bg.png";
// Start Button image

var startButtonWidth = 106;
var startButtonHeight = 35;
var startButtonX = Math.round((canvas.width-startButtonWidth)/2);
var startButtonY = Math.round((canvas.height-startButtonHeight)/2);
var startButtonReady = false;
var startButtonImage = new Image();
startButtonImage.onload = function() { startButtonReady = true; };
startButtonImage.src = "images/startButton.png";
// Start Button Hover image
var startButtonHoverReady = false;
var startButtonHoverImage = new Image();
startButtonHoverImage.onload = function() { startButtonHoverReady = true; };
startButtonHoverImage.src = "images/startButtonHover.png";
// Hero image
var heroReady = false;
var heroImage = new Image();
heroImage.onload = function () { heroReady = true;};
heroImage.src = "test.png";
// Monster image
var monsterReady = false;
var monsterImage = new Image();
monsterImage.onload = function() { monsterReady = true; };
monsterImage.src="images/pin48.png"
// Game objects
var hero = {
	speed: 256 // movement in pixels per second
};
var monster = {};
var monstersCaught = 0;
// Handle keyboard controls
var keysDown = {};
addEventListener("keydown",function(e){ keysDown[e.keyCode] = true;}, false);
addEventListener("keyup",function(e){ delete keysDown[e.keyCode];}, false);
// Handle mouse events
var mouseX;
var mouseY;
canvas.addEventListener("mousemove", function(e){mouseX = e.pageX - this.offsetLeft;mouseY = e.pageY - this.offsetTop;});
canvas.addEventListener("mouseup", checkClick);

function checkClick(mouseEvent) {
    if(started == false && mouseX > startButtonX && mouseY > startButtonY && mouseX < startButtonX+startButtonWidth && mouseY< startButtonY+startButtonHeight) {
        start = true;
        monstersCaught = 0;
        timeLeft = 45;
        reset();
        started = true;
        
//        canvas.removeEventListener("mouseup", checkClick);
    }
}

// Reset the game when the player catches a monster
var reset = function () {
	if(start) {
		hero.x = canvas.width / 2;
		hero.y = canvas.height / 2;
		start = false;
	}
	// Throw the monster somewhere on the screen randomly
	monster.x = 32 + (Math.random() * (canvas.width - 64));
	monster.y = 32 + (Math.random() * (canvas.height - 64));

	monster.x = 48 + (Math.random() * (canvas.width - 96));
	monster.y = 48 + (Math.random() * (canvas.height - 96));

};

// Update game objects
var update = function (modifier) {
     if(started == true) {
        timeLeft -= modifier;
    }
    if(timeLeft <= 0) {
        timeLeft = 0;
        started = false;
        start = true;
    }
    if( 83 in keysDown) {
        started = true;
        start = true;
    }
    if(started) {
        if( 82 in keysDown) {
            started = false;
            start = true;
            monstersCaught = 0;
            timeLeft = 60;
            reset();
            return;
        }
        if (38 in keysDown) { // Player holding up
            hero.y -= hero.speed * modifier;
            if(hero.y < 0) hero.y = 0;
        }
        if (40 in keysDown) { // Player holding down
            hero.y += hero.speed * modifier;
            if(hero.y > canvas.height-48) hero.y = canvas.height - 48;
        }
        if (37 in keysDown) { // Player holding left
            hero.x -= hero.speed * modifier;
            if(hero.x < 0) hero.x = 0;
        }
        if (39 in keysDown) { // Player holding right
            hero.x += hero.speed * modifier;
            if(hero.x > canvas.width-48) hero.x = canvas.width - 48;
        }
        // Are they touching?
        if ( hero.x <= (monster.x + 48) && monster.x <= (hero.x + 48) && hero.y <= (monster.y + 48) && monster.y <= (hero.y + 48)) {
            ++monstersCaught;
            reset();
        }
    }
};

// Draw everything
var render = function () {
	if (bgReady) {
		ctx.drawImage(bgImage, 0, 0);
	}
	if (heroReady) {
		ctx.drawImage(heroImage, hero.x, hero.y);
	}
	if (monsterReady) {
		ctx.drawImage(monsterImage, monster.x, monster.y);
	}
  // Score
	ctx.fillStyle = "rgb(0, 0, 0)";
	ctx.font = "24px Helvetica";
	ctx.textAlign = "left";
	ctx.textBaseline = "top";
	ctx.fillText("People Helped: " + monstersCaught, 16, 16);
    // Time Left
    ctx.fillStyle = "rgb(0, 0, 0)";
    ctx.font = "24px Helvetica";
    ctx.textAlign = "Left";
    ctx.textBaseline = "top";
    ctx.fillText("Time Left: " +timeLeft.toFixed(1), 340, 16);
    
    /*
    ctx.fillStyle = "rgb(0, 0, 0)";
    ctx.font = "24px Helvetica";
    ctx.textAlign = "Right";
    ctx.textBaseline = "top";
    ctx.fillText("" + mouseX +" " + mouseY, 340, 64);
    */

    if(started == false && startButtonReady && startButtonHoverReady) {
        ctx.fillStyle = "rgba(0,0,0,0.4)";
        ctx.fillRect(0,0,512,480);    
        if(mouseX > startButtonX && mouseY > startButtonY && mouseX < startButtonX+startButtonWidth && mouseY< startButtonY+startButtonHeight) {
            ctx.drawImage(startButtonHoverImage, startButtonX, startButtonY);
        } else {
            ctx.drawImage(startButtonImage, startButtonX, startButtonY);
        }
    }
};

// The main game loop
var main = function () {
	var now = Date.now();
	var delta = now - then;

	update(delta / 1000);
	render();
	then = now;
};

// Let's play this game!
reset();
var then = Date.now();
setInterval(main, 1); // Execute as fast as possible
